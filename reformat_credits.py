"""
Reformats The Movies Dataset credit CSV file at source to a JSON files list
of lists, where each outer list represents a movie, each inner list
represents the cast, and each element is an actor ID.

Usage:  python3 reformat_credits.py SRC DST
"""
import sys
import json
from ast import literal_eval
import unittest
from util import extract_csv_column, write_json

class TestReformatCredits(unittest.TestCase):
    """
    Tests for reformat credits.
    """
    def test_reformat_credits(self):
        """
        Tests the reformat credits function.
        """
        self.assertEqual(
            reformat_credits(
                [
                    json.dumps(
                        [
                            {
                                "id": 7
                            },
                            {
                                "id": 13
                            }
                        ]
                    ),
                    json.dumps(
                        [
                            {
                                "id": 3
                            }
                        ]
                    ),
                    json.dumps(
                        [
                        ]
                    ),
                ],
            ),
            [[7,13],[3],[]]
        )

def reformat_credits(casts):
    """
    Reformats a list of casts in The Movies Dataset format to a list of movies,
    where each movie is a list of actor IDs in the cast.
    """
    return [[member["id"] for member in literal_eval(cast_string)]
            for cast_string in casts]

if __name__ == "__main__":
    # TODO: CLI error handling and usage mesesage.
    write_json(sys.argv[2], reformat_credits(extract_csv_column(sys.argv[1], "cast")))
