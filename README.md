## Usage
Run `python3 bacon.py`.

JSON request:
`{"actor_id": the ID of the actor to get a Bacon number for}`

JSON response:
`{"bacon_number": the Bacon number for the given actor ID}`

## Updating LUT
* Download credits from The Movies Dataset (https://www.kaggle.com/rounakbanik/the-movies-dataset).
* Reformat the credits:  `python3 reformat_credits.py data/credits.csv data/casts.json`, where `data/credits.csv` is the file that you downloaded.
* Calculate the degrees of separation from Kevin Bacon:  `python3 degrees_of_separation.py data/casts.json 4724 data/bacon.json`
