"""
Utility to convert a list of groups to a degress of separation map for a given
starting node ID.  Usage:  python3 degrees_of_separation SRC ZERO_ID DST.

The input format is a JSON list of lists, where the outer list is a list of 
groups, the inner list is a list of node IDs, and each element is an integer
node ID.  The output format is a JSON object with node ID keys and degrees
of separation values.
"""
import sys
from collections import deque
import unittest
from util import write_json, read_json

class TestDegreesOfSeparation(unittest.TestCase):
    """
    Tests for degrees of separation.
    """
    def test_degrees_of_separation(self):
        dos_map = degrees_of_separation_map(
            [
                [1,2,3],
                [3,4,5],
                [6,7,8],
                [1,9],
                [9,10],
            ],
            4
        )
        self.assertEqual(dos_map.get(4), 0)
        self.assertEqual(dos_map.get(3), 1)
        self.assertEqual(dos_map.get(2), 2)
        self.assertEqual(dos_map.get(10), 4)
        self.assertEqual(dos_map.get(6), None)


def degrees_of_separation_map(data, zero_id):
    """
    Creates a mapping of nodes to their degree of separation from the given
    node with ID zero_id.  data is expected to be a list of lists, where the
    outer list represents a list of node groups, the inner list represents
    a single group, and each element is a node ID.  Note that any node that
    is not reachable from the zero_id node will not be in the returned mapping.
    """
    # Initial degrees of separation result
    result = {zero_id: 0}

    # Figure out which groups each node belongs for propagating side effects
    node_to_group_map = {node: [] for group in data for node in group}
    for group_index, group in enumerate(data):
        for node in group:
            node_to_group_map[node].append(group_index)

    # Use a queue to track which groups we need to visit.  Also, keep track of
    # which groups are in the queue to avoid adding them multiple times.
    queue = deque()
    for group_id, _ in enumerate(data):
        queue.append(group_id)
    in_queue = {group_id: True for group_id in queue}

    def revisit(group):
        """
        Called to denote that the given group should be visited again because
        one of the contained members changed.
        """
        if not in_queue[group]:
            in_queue[group] = True
            queue.append(group)

    def update_node(node, distance):
        """
        Called to denote that it is possible to reach the given node with the
        given distance.
        """
        if node not in result or result[node] > distance:
            result[node] = distance
            for group in node_to_group_map[node]:
                revisit(group)

    # (adaptation of Djistra's algorithm for multiple starting points)
    while len(queue) > 0:
        group_id = queue.popleft()
        group = data[group_id]
        lowest_distance = min((result[node] for node in group if node in result), default=None)
        if lowest_distance is not None:
            distance = lowest_distance + 1
            for node in group:
                update_node(node, distance)
        in_queue[group_id] = False

    return result

if __name__ == "__main__":
    # TODO: CLI error handling and usage message.
    write_json(sys.argv[3], degrees_of_separation_map(read_json(sys.argv[1]), int(sys.argv[2])))
