"""
Simple module for an API that returns the Bacon number for a given actor ID from
The Movies Dataset.

JSON request:
{
  "actor_id": the ID of the actor to get a Bacon number for
}

JSON response:
{
  "bacon_number": the Bacon number for the given actor ID
}
"""

from flask import Flask, jsonify, request
from util import read_json
import json
app = Flask(__name__)

bacon_map = {
    int(actor_id): bacon_number \
    for actor_id, bacon_number in read_json("data/bacon.json").items()
}

@app.route('/', methods=["GET", "POST"])
def bacon_number():
    """
    Bacon number API handler.
    """
    return jsonify({
        "bacon_number": bacon_map.get(request.get_json()["actor_id"])
    })

if __name__ == "__main__":
    app.run()
