"""
Test module.  In practice
"""
from util import TestUtil
from reformat_credits import TestReformatCredits
from degrees_of_separation import TestDegreesOfSeparation
import unittest

if __name__ == "__main__":
    unittest.main()
