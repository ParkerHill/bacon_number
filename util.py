import csv
import json
import unittest

class TestUtil(unittest.TestCase):
    """
    Tests for utility functions.
    """
    def test_extract_csv_column(self):
        """
        Tests the basic behavior of extracting a CSV column.
        """
        self.assertEqual(extract_csv_column("tests/column_test.csv", "h2"), ["v12", "v22", "v32"])

    def test_json_write_read(self):
        """
        Tests the basic write/read JSON behavior.
        """
        test_json = {
            "key": [
                1,
                2.0,
                "3"
            ]
        }
        write_json("tests/write_json.json", test_json)
        assert(read_json("tests/write_json.json") == test_json)


def extract_csv_column(path, column_name):
    """
    Extracts the column_name named column from the CSV files at the given path.
    """
    with open(path) as csvfile:
        rows = csv.reader(csvfile)
        column_index = next(rows).index(column_name)
        return [row[column_index] for row in rows]


def read_json(path):
    """
    Reads a JSON file from the given path.
    """
    with open(path) as jsonfile:
        return json.load(jsonfile)

def write_json(path, data):
    """
    Writes a JSON file to the given path with the given data.
    """
    with open(path, "w") as jsonfile:
        json.dump(data, jsonfile)
